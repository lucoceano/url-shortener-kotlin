package com.lucoceano.shortener

import com.lucoceano.shortener.models.ShortLink
import com.lucoceano.shortener.storages.ShortLinkStorage

class LocalShortLinkStorage : ShortLinkStorage {

    private val links: MutableList<ShortLink> = mutableListOf()

    override fun saveLink(link: ShortLink) {
        links.add(link)
    }

    override fun getLinkById(id: String): ShortLink? {
        return links.find { it.id == id }
    }
}
