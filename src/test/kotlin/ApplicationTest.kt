package com.lucoceano.shortener

import com.google.gson.Gson
import com.lucoceano.shortener.apis.LinkApi
import com.lucoceano.shortener.apis.RedirectApi
import com.lucoceano.shortener.models.ShortLink
import com.lucoceano.shortener.services.NanoIdService
import com.lucoceano.shortener.services.IdService
import com.lucoceano.shortener.services.ShortLinkService
import io.ktor.application.*
import io.ktor.features.ContentNegotiation
import io.ktor.gson.GsonConverter
import io.ktor.http.*
import io.ktor.routing.*
import io.ktor.server.testing.*
import kotlin.test.*

class ApplicationTest {

    private fun configureApp(application: Application, service: ShortLinkService) {
        application.install(ContentNegotiation) {
            register(ContentType.Application.Json, GsonConverter())
        }

        application.install(Routing) {
            LinkApi(service)
            RedirectApi(service)
        }
    }

    @Test
    fun testLinksNotFound() = withTestApplication {
        val idService = NanoIdService()
        val storage = LocalShortLinkStorage()
        val service = ShortLinkService(storage, idService)
        configureApp(application, service)

        val link = ShortLink("some-id", "some-url")
        storage.saveLink(link)

        with(handleRequest(HttpMethod.Get, "/links/some-id") {}) {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals(Gson().toJson(link), response.content)
        }
    }

    @Test
    fun testLinkCreation() = withTestApplication {
        val randId = "gaA9FB8VYzs5Cn9XDgb3s"
        val idService = object : IdService {
            override fun get(): String {
                return randId
            }
        }
        configureApp(application, ShortLinkService(LocalShortLinkStorage(), idService))

        with(handleRequest(HttpMethod.Post, "/links") {
            addHeader("Content-Type", "application/json")
            setBody("""{"url":"http://google.com"}""")
        }
        ) {
            assertEquals(HttpStatusCode.Created, response.status())
            assertEquals("""{"id":"$randId","url":"http://google.com"}""",
                    response.content
            )
        }
    }

    @Test
    fun testLinkCreationNoURL() = withTestApplication {
        configureApp(application, ShortLinkService(LocalShortLinkStorage(), NanoIdService()))

        with(
                handleRequest(HttpMethod.Post, "/links") {
                    addHeader("Content-Type", "application/json")
                    setBody("{\"url\": null}")
                }
        ) { assertEquals(HttpStatusCode.BadRequest, response.status()) }
    }
}
