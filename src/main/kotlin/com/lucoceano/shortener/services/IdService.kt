package com.lucoceano.shortener.services

interface IdService {
    fun get(): String
}
