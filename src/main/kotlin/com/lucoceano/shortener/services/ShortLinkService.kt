package com.lucoceano.shortener.services

import com.github.benmanes.caffeine.cache.Cache
import com.github.benmanes.caffeine.cache.Caffeine
import com.lucoceano.shortener.models.ShortLink
import com.lucoceano.shortener.storages.ShortLinkStorage
import java.util.concurrent.TimeUnit

class ShortLinkService(private val storage: ShortLinkStorage, private val idService: IdService) {
  private val cache: Cache<String, ShortLink> =
      Caffeine.newBuilder().expireAfterWrite(1, TimeUnit.DAYS).maximumSize(100000).build()

  fun get(id: String): ShortLink? {
    return cache.getIfPresent(id) ?: getAndCache(id)
  }

  fun create(url: String): ShortLink {
    val id = idService.get()
    val shortLink = ShortLink(id, url)
    storage.saveLink(shortLink)
    return shortLink
  }

  private fun getAndCache(id: String): ShortLink? {
    return storage.getLinkById(id)?.also { cache.put(id, it) }
  }
}
