package com.lucoceano.shortener.services

import com.aventrix.jnanoid.jnanoid.NanoIdUtils

class NanoIdService : IdService {
    override fun get(): String {
        return NanoIdUtils.randomNanoId()
    }
}
