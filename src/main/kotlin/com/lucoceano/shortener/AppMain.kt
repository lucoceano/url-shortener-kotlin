package com.lucoceano.shortener

import com.codahale.metrics.Slf4jReporter
import com.lucoceano.shortener.apis.LinkApi
import com.lucoceano.shortener.apis.RedirectApi
import com.lucoceano.shortener.services.NanoIdService
import com.lucoceano.shortener.services.ShortLinkService
import com.lucoceano.shortener.storages.PostgreShortLinkStorage
import com.lucoceano.shortener.factories.PostgreDatabaseFactory
import com.lucoceano.shortener.factories.DatabaseConfig
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.metrics.dropwizard.*
import io.ktor.routing.*
import io.ktor.util.*
import java.util.concurrent.TimeUnit

@KtorExperimentalAPI
fun Application.main() {
    install(DefaultHeaders)
    install(DropwizardMetrics) {
        val reporter =
                Slf4jReporter.forRegistry(registry)
                        .outputTo(log)
                        .convertRatesTo(TimeUnit.SECONDS)
                        .convertDurationsTo(TimeUnit.MILLISECONDS)
                        .build()
        reporter.start(10, TimeUnit.SECONDS)
    }
    install(ContentNegotiation) { register(ContentType.Application.Json, GsonConverter()) }
    install(
            Compression,
            ApplicationCompressionConfiguration()
    ) // see https://ktor.io/docs/compression.html
    install(HSTS, ApplicationHstsConfiguration()) // see https://ktor.io/docs/hsts.html

    PostgreDatabaseFactory.init(DatabaseConfig())

    val idService = NanoIdService()
    val storage = PostgreShortLinkStorage()
    val service = ShortLinkService(storage, idService)
    install(Routing) {
        LinkApi(service)
        RedirectApi(service)
    }
}
