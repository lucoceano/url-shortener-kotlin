package com.lucoceano.shortener.factories

import com.lucoceano.shortener.storages.tables.LinksTable
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

object PostgreDatabaseFactory : DatabaseFactory {

  override fun init(config: DatabaseConfig) {
    val hirakiConfig = HikariConfig()
    hirakiConfig.jdbcUrl = "jdbc:postgresql://${config.host}/default_database"
    hirakiConfig.username = config.username
    hirakiConfig.password = config.password

    Database.connect(HikariDataSource(hirakiConfig))
    transaction { SchemaUtils.create(LinksTable) }
  }
}
