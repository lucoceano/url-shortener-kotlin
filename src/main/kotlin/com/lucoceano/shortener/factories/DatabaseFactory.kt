package com.lucoceano.shortener.factories

interface DatabaseFactory {
  fun init(config: DatabaseConfig)
}
