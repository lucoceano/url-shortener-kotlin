package com.lucoceano.shortener.factories

data class DatabaseConfig(
                val host: String = System.getenv("DB_HOST") ?: "localhost:5432",
                val username: String = System.getenv("DB_USERNAME") ?: "username",
                val password: String = System.getenv("DB_PASSWORD") ?: "password"
)
