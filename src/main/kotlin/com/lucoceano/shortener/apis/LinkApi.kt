package com.lucoceano.shortener.apis

import com.lucoceano.shortener.models.Link
import com.lucoceano.shortener.services.ShortLinkService
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.receiveOrNull
import io.ktor.response.*
import io.ktor.routing.*

fun Route.LinkApi(service: ShortLinkService) {
    route("/links/{id}") {
        get {
            val id = call.parameters["id"] ?: return@get call.respond(HttpStatusCode.BadRequest)

            val shortLink = service.get(id) ?: return@get call.respond(HttpStatusCode.NotFound)

            return@get call.respond(shortLink)
        }
    }

    route("/links") {
        post {
            val link =
                    call.receiveOrNull<Link>()
                            ?: return@post call.respond(HttpStatusCode.BadRequest)

            val url = link.url ?: return@post call.respond(HttpStatusCode.BadRequest)

            val shortLink = service.create(url)
            return@post call.respond(HttpStatusCode.Created, shortLink)
        }
    }
}
