package com.lucoceano.shortener.apis

import com.lucoceano.shortener.services.ShortLinkService
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.RedirectApi(service: ShortLinkService) {
    route("/redirects/{linkId}") {
        get {
            val linkId =
                    call.parameters["linkId"] ?: return@get call.respond(HttpStatusCode.BadRequest)
            val shortLink = service.get(linkId) ?: return@get call.respond(HttpStatusCode.NotFound)

            call.response.headers.append("Location", shortLink.url)
            call.respond(HttpStatusCode.MovedPermanently)
        }
    }
}
