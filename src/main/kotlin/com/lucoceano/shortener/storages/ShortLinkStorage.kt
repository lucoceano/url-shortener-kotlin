package com.lucoceano.shortener.storages

import com.lucoceano.shortener.models.ShortLink

interface ShortLinkStorage {
    fun saveLink(link: ShortLink)
    fun getLinkById(id: String): ShortLink?
}