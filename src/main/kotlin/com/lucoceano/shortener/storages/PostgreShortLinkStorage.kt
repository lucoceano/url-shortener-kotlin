package com.lucoceano.shortener.storages

import com.lucoceano.shortener.models.ShortLink
import com.lucoceano.shortener.storages.tables.LinksTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class PostgreShortLinkStorage : ShortLinkStorage {

    override fun saveLink(link: ShortLink) {
        transaction {
            LinksTable.insert {
                it[id] = link.id
                it[url] = link.url
            }
        }
    }

    override fun getLinkById(id: String): ShortLink? {
        return transaction { LinksTable.select { LinksTable.id eq id }.map { it.toShortLink() }.firstOrNull() }
    }

    private fun ResultRow.toShortLink(): ShortLink {
        return ShortLink(this[LinksTable.id], this[LinksTable.url])
    }
}
