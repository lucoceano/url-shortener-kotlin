package com.lucoceano.shortener.storages.tables

import org.jetbrains.exposed.sql.Table

object LinksTable : Table() {
    val id = varchar("id", 21)
    val url = varchar("title", 8000)

    override val primaryKey = PrimaryKey(id, name = "PK_links_ID")
}